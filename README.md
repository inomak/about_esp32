# Detalhes sobre o ESP32

## Pinagem do chip [aqui](https://bitbucket.org/inomak/about_esp32/src/master/chip_pinout.png)

## Pinagem da placa ESP32 [aqui](https://bitbucket.org/inomak/about_esp32/src/master/dev_kit_pinout.png)

## Limites de uso de pinos
Veja a tabela abaixo para saber em que cada pino pode ser utilizado ou nao:

| GPIO | PWM | ANALOG READ | PULL_UP | OUTPUT | INPUT | - |
| :----: | :----: | :----: | :----: | :----: | :----: | :----: |
| OK | OK | OK | OK | TALVEZ | - |
| 1 | - | - | - | - | - | - |
| 2 | OK | OK | OK | OK | OK | - |
| 3 | - | - | - | - | - | - |
| 4 | OK | OK | OK | OK | OK | - |
| 5 | OK | - | OK | OK | OK | - |
| 6 | - | - | - | - | - | - |
| 7 | - | - | - | - | - | - |
| 8 | - | - | - | - | - | - |
| 9 | - | - | - | - | - | - |
| 10 | - | - | - | - | - | - |
| 11 | - | - | - | - | - | - |
| 12 | OK | OK | - | OK | OK | - |
| 13 | OK | OK | OK | OK | OK | - |
| 14 | OK | OK | OK | OK | OK | - |
| 15 | OK | OK | OK | OK | OK | - |
| 16 | OK | - | OK | OK | OK | - |
| 17 | OK | - | OK | OK | OK | - |
| 18 | OK | - | OK | OK | OK | - |
| 19 | OK | - | OK | OK | OK | - |
| 20 | - | - | - | - | - | - |
| 21 | OK | - | OK | OK | OK | - |
| 22 | OK | - | OK | OK | OK | - |
| 23 | OK | - | OK | OK | OK | - |
| 24 | - | - | - | - | - | - |
| 25 | OK | OK | OK | OK | OK | - |
| 26 | OK | OK | OK | OK | OK | - |
| 27 | OK | OK | OK | OK | OK | - |
| 28 | - | - | - | - | - | - |
| 29 | - | - | - | - | - | - |
| 30 | - | - | - | - | - | - |
| 31 | - | - | - | - | - | - |
| 32 | OK | OK | OK | OK | OK | - |
| 33 | OK | OK | OK | OK | OK | - |
| 34 | - | OK | - | - | OK | - |
| 35 | - | OK | - | - | OK | - |
| 36 | - | OK | - | - | OK | - |
| 37 | - | - | - | - | - | - |
| 38 | - | - | - | - | - | - |
| 39 | - | OK | - | - | OK | - |
| 40 | - | - | - | - | - | - |


## Limitacoes dos pinos analogicos
O ESP32 possui 2 canais de sinais analogicos para escrita sendo eles o ADC1 e o ADC2, o canal ADC1 vai do pino 32 ao 39. O canal ADC2 sao os pinos 0, 2, 4, 10, 12, 15, 25 ~ 27.
Porem o canal ADC2 e utilizado pelo driver do WI-FI, ou seja, apenas pode-se utilizar os canais do ADC2 como escrita analogica quando o driver WI-FI nao for utilizado.

## PWM
O ESP32 possui 16 canais de PWM independentes, todos os pinos podem ser utilizados como PWM exceto os pinos 34 ao 39.

## Enable (EN)
O pino **enable** e conectado ao pino do regulador 3.3V, caso seja pressionado ao GND ele ira desligar o regulador 3.3V. Ele pode ser conectado a um pushbutton para reiniciar seu ESP32.

